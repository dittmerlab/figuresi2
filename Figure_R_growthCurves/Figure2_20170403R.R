# libraries ----
library(gdata)
library(vioplot)
library(ggplot2)
library(reshape2)
library(beeswarm)
library(multcomp)
library(RColorBrewer)
library(lme4)

# options ----
options(digits=3)

getwd()
# setwd("/Users/dirk/Dropbox/801 X Myc/Figure IgG R")


# functions 
AnscombeTransform <- function(exosome){
  exosome$pfuAnscombe <- 2 * sqrt(exosome$pfu + 3/8)
  return(exosome)
}
dataCleaning <- function(exosome){
  head(exosome)
  ncol(exosome)
  colnames(exosome)

  
  summary(exosome)
  colnames(exosome)
  
  # exosome$DATE <- as.factor(exosome$DATE)
  
  #levels(exosome$Sample)
  #exosome <- exosome[exosome$Sample != "", ]
  
  exosome <- na.omit(exosome) 
  exosome <- drop.levels(exosome)
  
  levels(exosome$condition)
  exosome$condition <- relevel(exosome$condition, ref = "parent")

  colnames(exosome)

  
  return(exosome)
}


addFloorZero <- function(exosomeAve){
  exosomeAve[exosomeAve$cp <= 0,"cp"] <- 1 
  return(exosomeAve)
}

figure1 <- function(exosome){
  
  a <-ggplot(aes(x = time, y = cp, color = class), 
             data = exosome) 
  #a <- a + geom_boxplot(aes(x = timeF, y = cp, color = class), 
   #                     fill = "gray",
    #                    outlier.colour = "white")
  a <- a + geom_point (size = I(7), 
                       alpha = I(0.5),
                       position = position_jitter(w = 0.1, h = 0.0))

  a <- a + scale_colour_brewer(type = "qual", palette = "Set1")
  a <- a + theme_bw() 
  a <- a + xlab ("\n time") 
  a <- a + ylab ("Log10(RLU)") 
  a <- a + labs(title = "Growth\n")
  
  # a <- a + scale_y_log10(limits = c(0.1, 1000), breaks=c(0.1,1,10,100,100))
  
  
  # a <- a + facet_wrap( ~ genotype, scales = "free_y")  
  
  a <- a + theme(legend.position = "right") 
  a <- a + theme(strip.background = element_rect(colour = "white", fill = "white"))   
  a <- a + theme(axis.text.x = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.x = element_text(size = 24))
  a <- a + theme(axis.text.y = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.y = element_text(size = 24, angle = 90)) 
  a <- a + theme(plot.title = element_text(size = 24))  
  a <- a  + theme(strip.text.x = element_text(size = 24, vjust = 1))
  a <- a  + theme(strip.text.y = element_text(size = 24))
  a <- a + theme(panel.grid.minor = element_blank()) 
  a <- a + theme(panel.grid.major = element_blank())
  return (a)
}

figure2 <- function(rain){
  
  a <-ggplot(aes(x = time, y = cp, color = class), 
             data = rain) 
  #a <- a + geom_boxplot(aes(x = timeF, y = cp, color = class), 
  #                     fill = "gray",
  #                    outlier.colour = "white")
  a <- a + geom_point (size = I(4), 
                       alpha = I(0.5),
                       position = position_jitter(w = 0.1, h = 0.0))
  a <- a + geom_smooth(method = "rlm")
  a <- a + scale_colour_brewer(type = "qual", palette = "Set1")
  a <- a + facet_grid(. ~ cell)
  a <- a + theme_bw() 
  a <- a + xlab ("\n time in hrs") 
  a <- a + ylab ("Log10(CellTiterGlow)\n") 
  a <- a + labs(title = "Growth\n")
  
  # a <- a + scale_y_log10(limits = c(0.1, 1000), breaks=c(0.1,1,10,100,100))
  
  
  # a <- a + facet_wrap( ~ genotype, scales = "free_y")  
  
  a <- a + theme(legend.position = "right") 
  a <- a + theme(strip.background = element_rect(colour = "white", fill = "white"))   
  a <- a + theme(axis.text.x = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.x = element_text(size = 24))
  a <- a + theme(axis.text.y = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.y = element_text(size = 24, angle = 90)) 
  a <- a + theme(plot.title = element_text(size = 24))  
  a <- a  + theme(strip.text.x = element_text(size = 24, vjust = 1))
  a <- a  + theme(strip.text.y = element_text(size = 24))
  a <- a + theme(panel.grid.minor = element_blank()) 
  a <- a + theme(panel.grid.major = element_blank())
  return (a)
}

figure3 <- function(rain){
  a <-ggplot(aes(x = timeF, y = cp, color = class), 
             data = rain) 
  a <- a + geom_boxplot(aes(x = timeF, y = cp, color = class), 
                       fill = "gray",
                      outlier.colour = "white")
  
  a <- a + scale_colour_brewer(type = "qual", palette = "Set1")
  a <- a + theme_bw() 
  a <- a + xlab ("\n time in hrs") 
  a <- a + ylab ("Log10(RLU)") 
  a <- a + labs(title = "Growth\n")

  a <- a + theme(legend.position = "right") 
  a <- a + theme(strip.background = element_rect(colour = "white", fill = "white"))   
  a <- a + theme(axis.text.x = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.x = element_text(size = 24))
  a <- a + theme(axis.text.y = element_text(size = 24, angle = 0)) 
  a <- a + theme(axis.title.y = element_text(size = 24, angle = 90)) 
  a <- a + theme(plot.title = element_text(size = 24))  
  a <- a  + theme(strip.text.x = element_text(size = 24, vjust = 1))
  a <- a  + theme(strip.text.y = element_text(size = 24))
  a <- a + theme(panel.grid.minor = element_blank()) 
  a <- a + theme(panel.grid.major = element_blank())
  return (a)
}

# DATA READ ----
# 
exosome <- read.delim("Figure_R_growthCurves/toR.txt")
summary(exosome)
head(exosome)


# SAFE COPY OF DATASET ----
rainall <- exosome
# write.table(rainall,file = "CompleteDataSet.txt", sep = "\t")
exosome <- rainall


# Data cleaning ----
exosome <- dataCleaning(exosome)
summary(exosome)

exosome <- exosome[exosome$time > 0,]
exosome <- drop.levels(exosome)

colnames(exosome)
exosome$timeF <- as.factor(as.character(exosome$time))

summary(exosome)
exosome <- exosome[,2:ncol(exosome)]
exosome <- melt(exosome)

summary(exosome)
colnames(exosome)
colnames(exosome)[6] <- "cp"
colnames(exosome)[3] <- "class"
exosome$cp <- log10(exosome$cp) 

exosome$time <- as.numeric(as.character(exosome$timeF))

summary(exosome)
exosome <- exosome[exosome$class != "start",]
exosome <- drop.levels(exosome)

#
# Figure 1 ---
# figure1(addFloorZero(exosome))
figure2(addFloorZero(exosome))
# figure3(addFloorZero(exosome))

#png(file = "plot1.png", 
  #  units = "in", 
  #  width = 10, 
   # height = 8, res = 600)
#figure2(addFloorZero(exosome))
#dev.off()

# Multiple comparison ----
rain <- exosome
rain <- drop.levels(rain)
summary(rain)

colnames(rain)
rain$time <- as.factor(as.character(rain$time))
rain <- drop.levels(rain)

rain <- melt(rain)
colnames(rain)
colnames(rain)[8] <- "cp"

rain$time <- as.numeric(as.character(rain$time))
rain$cp <- log10(rain$cp)
rain$class <- relevel(rain$class, ref = "parent")


# linear model
result <- lm(cp ~ class + cell * time, data = rain)
summary(result)
anova(result)
pf(q=47.01, df1=5, df2=12, lower.tail=FALSE)


# anova
summary(rain)
result <- aov(cp ~ class + cell + time, 
              data = rain)
summary(result)
TukeyHSD(result)$class

# random effects model
# does not yet account for repeated measurements over time
#

result <- lmer(cp ~ class + time + (1|cell), 
              data = rain)
result.pair <- glht(result, linfct = mcp(class="Tukey"))
summary(result.pair)

result.pair <- glht(result, linfct = mcp(class="Dunnett"))
summary(result.pair)
confint(result.pair, 
        level = 0.95)

plot(result.pair)

